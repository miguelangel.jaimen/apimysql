'use strict';
module.exports = (sequelize, DataTypes) => {
  const ModeloUbicacionZona = sequelize.define('ModeloUbicacionZona', {
    id_ubicacion_zona: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    codigo_zona: DataTypes.STRING,
    id_modelo_ubicacion: DataTypes.STRING,
    estado_zona: DataTypes.INTEGER
  },  {tableName:'tbl_modelo_ubicacion_zona', timestamps: false});
  ModeloUbicacionZona.associate = function(models) {
  
  // // ZONA
  ModeloUbicacionZona.belongsTo(models.Zona, {
    foreignKey: 'codigo_zona',
    as: 'zona',
  });

  ModeloUbicacionZona.hasMany(models.ModeloZonaCategoria, {
    foreignKey: 'id_ubicacion_zona',
    as: 'categorias',
  });
  
  // // MODELO_UBICACION
  ModeloUbicacionZona.belongsTo(models.ModeloUbicacion, {
    foreignKey: 'id_modelo_ubicacion',
    as: 'modelo_ubicacion',
  });

  // // PRIVILEGIOS
  //   ModeloUbicacionZona.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   ModeloUbicacionZona.hasMany(models.ModeloUbicacionZonaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   ModeloUbicacionZona.hasMany(models.ModeloUbicacionZonaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacion_zonaesUsuario',
  //   });
  };
 

  return ModeloUbicacionZona;
};