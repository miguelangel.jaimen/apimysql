'use strict';
module.exports = (sequelize, DataTypes) => {
  const Etapa = sequelize.define('Etapa', {
    id_etapa: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_proyecto: DataTypes.STRING,
    nombre_etapa: DataTypes.STRING
  },  {tableName:'tbl_etapas', timestamps: false});
  Etapa.associate = function(models) {
  
  // // VSITAS
  // Etapa.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // PROYECTO
  Etapa.belongsTo(models.Proyecto, {
    foreignKey: 'id_proyecto',
    as: 'proyecto',
  });

  // // PRIVILEGIOS
    Etapa.hasMany(models.Sector, {
      foreignKey: 'id_etapa',
      as: 'sectores',
    });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Etapa.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Etapa.hasMany(models.EtapaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'etapaesUsuario',
  //   });
  };
 

  return Etapa;
};