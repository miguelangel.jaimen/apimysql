'use strict';
module.exports = (sequelize, DataTypes) => {
  const UbicacionZona = sequelize.define('UbicacionZona', {
    id_ubicacion_zona: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    codigo_ubicacion: DataTypes.STRING,
    codigo_zona: DataTypes.STRING
  },  {tableName:'tbl_ubicacion_zonas', timestamps: false});
  UbicacionZona.associate = function(models) {
  
  
  // // UBIACION
  UbicacionZona.belongsTo(models.Ubicacion, {
    foreignKey: 'codigo_ubicacion',
    as: 'ubicacion',
  });

    // // ZONA
  UbicacionZona.belongsTo(models.Zona, {
    foreignKey: 'codigo_zona',
    as: 'zona',
  });
  // UbicacionZona.belongsToMany(codigo_zona, { through: ZonaCategoria });
  // UbicacionZona.hasMany(models.ZonaCategoria, {
  //   foreignKey: 'codigo_zona',
  //   as: 'zonas',
  // });
  
  

  // // // MODELO_UBICACION
  // UbicacionZona.belongsTo(models.ModeloUbicacion, {
  //   foreignKey: 'id_modelo_ubicacion',
  //   as: 'modelo_ubicacion',
  // });

  // // PRIVILEGIOS
  //   UbicacionZona.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   UbicacionZona.hasMany(models.UbicacionZonaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   UbicacionZona.hasMany(models.UbicacionZonaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacion_zonaesUsuario',
  //   });
  };
 

  return UbicacionZona;
};