'use strict';
module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define('Log', {
    id_log: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    contexto: DataTypes.STRING,
    id_contexto: DataTypes.STRING,
    id_usuario: DataTypes.STRING,
    fecha_log: DataTypes.STRING,
    tipo_log: DataTypes.STRING,
    
  },  {tableName:'tbl_logs', timestamps: false});
  Log.associate = function(models) {
  
  // // VSITAS
  // Log.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  // Log.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Log.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Log.hasMany(models.LogUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Log.hasMany(models.LogUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'logesUsuario',
  //   });
  };
 

  return Log;
};