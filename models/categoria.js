'use strict';
module.exports = (sequelize, DataTypes) => {
  const Categoria = sequelize.define('Categoria', {
    codigo_categoria: {type: DataTypes.STRING, primaryKey: true},
    nombre_categoria: DataTypes.STRING
  },  {tableName:'tbl_categorias', timestamps: false});
  Categoria.associate = function(models) {

  // // ZONA_CATEGORIA
    Categoria.hasMany(models.ModeloZonaCategoria, {
      foreignKey: 'codigo_categoria',
      as: 'zonas_categoria',
    });

  // // ZONA_CATEGORIA
  Categoria.hasMany(models.CategoriaFalla, {
    foreignKey: 'codigo_categoria',
    as: 'fallas',
  });

  };

 

  return Categoria;
};