'use strict';
module.exports = (sequelize, DataTypes) => {
  const Extencion = sequelize.define('Extencion', {
    id_extencion: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_bien: DataTypes.STRING,
    codigo_ubicacion: DataTypes.STRING,
    codigo_zona: DataTypes.STRING,
    codigo_categoria: DataTypes.STRING
  },  {tableName:'tbl_extenciones_bien', timestamps: false});
  Extencion.associate = function(models) {
  
  // // VSITAS
  Extencion.belongsTo(models.Bien, {
    foreignKey: 'id_bien',
    as: 'bienes',
  });
  // // VSITAS
  Extencion.belongsTo(models.Ubicacion, {
    foreignKey: 'codigo_ubicacion',
    as: 'ubicaciones',
  });
  // // VSITAS
  Extencion.belongsTo(models.Zona, {
    foreignKey: 'codigo_zona',
    as: 'zonas',
  });
  // // VSITAS
  Extencion.belongsTo(models.Categoria, {
    foreignKey: 'codigo_categoria',
    as: 'categorias',
  });

  // // PRIVILEGIOS
  //   Extencion.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Extencion.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Extencion.hasMany(models.ExtencionUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'extencionesUsuario',
  //   });
  };
 

  return Extencion;
};