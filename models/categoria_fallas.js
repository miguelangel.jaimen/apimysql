'use strict';
module.exports = (sequelize, DataTypes) => {
  const CategoriaFalla = sequelize.define('CategoriaFalla', {
    id_categoria_falla: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    codigo_categoria: DataTypes.STRING,
    codigo_falla: DataTypes.STRING
  },  {tableName:'tbl_categoria_fallas', timestamps: false});
  CategoriaFalla.associate = function(models) {
  
  // // CATEGORIA
  CategoriaFalla.belongsTo(models.Categoria, {
    foreignKey: 'codigo_categoria',
    as: 'categoria',
  });

  // // FALLA
  CategoriaFalla.belongsTo(models.Falla, {
    foreignKey: 'codigo_falla',
    as: 'falla',
  });
 
//   // // UBIACION
//   CategoriaFalla.belongsTo(models.falla, {
//     foreignKey: 'codigo_falla',
//     as: 'falla',
//   });

  // CategoriaFalla.hasMany(models.CategoriaFalla, {
  //   foreignKey: 'id_ubicacion_zona',
  //   as: 'categorias',
  // });
  
  // // // MODELO_UBICACION
  // CategoriaFalla.belongsTo(models.ModeloUbicacion, {
  //   foreignKey: 'id_modelo_ubicacion',
  //   as: 'modelo_ubicacion',
  // });

  // // PRIVILEGIOS
  //   CategoriaFalla.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   CategoriaFalla.hasMany(models.UbicacionZonaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   CategoriaFalla.hasMany(models.UbicacionZonaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacion_zonaesUsuario',
  //   });
  };
 

  return CategoriaFalla;
};