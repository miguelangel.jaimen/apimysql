'use strict';
module.exports = (sequelize, DataTypes) => {
  const Zona = sequelize.define('Zona', {
    codigo_zona: {type: DataTypes.STRING, primaryKey: true},
    nombre_zona: DataTypes.STRING,
  },  {tableName:'tbl_zonas', timestamps: false});
  Zona.associate = function(models) {

  // // CATEGORIAS
    Zona.hasMany(models.ZonaCategoria, {
      foreignKey: 'codigo_zona',
      as: 'categorias',
    });
  
  // // VSITAS
  // Zona.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  // Zona.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Zona.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Zona.hasMany(models.ZonaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Zona.hasMany(models.ZonaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'zonaesUsuario',
  //   });
  };
 

  return Zona;
};