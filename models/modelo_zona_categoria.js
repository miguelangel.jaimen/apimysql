'use strict';
module.exports = (sequelize, DataTypes) => {
  const ModeloZonaCategoria = sequelize.define('ModeloZonaCategoria', {
    id_zona_categoria: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_ubicacion_zona: DataTypes.STRING,
    codigo_categoria: DataTypes.STRING,
    estado_categoria: DataTypes.INTEGER
  },  {tableName:'tbl_modelo_zona_categoria', timestamps: false});
  ModeloZonaCategoria.associate = function(models) {
  
  // // CATEGORIA
  // ModeloZonaCategoria.hasMany(models.Categoria, {
  //   foreignKey: 'codigo_categoria',
  //   as: 'categoria',
  // });
  
  // // UBICACION_ZONA
  ModeloZonaCategoria.belongsTo(models.Categoria, {
    foreignKey: 'codigo_categoria',
    as: 'categoria',
  });

  ModeloZonaCategoria.belongsTo(models.ModeloUbicacionZona, {
    foreignKey: 'id_ubicacion_zona',
    as: 'ubicacion_zona',
  });

  // // PRIVILEGIOS
  //   ModeloZonaCategoria.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   ModeloZonaCategoria.hasMany(models.ZonaCategoriaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   ModeloZonaCategoria.hasMany(models.ZonaCategoriaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'zona_categoriaesUsuario',
  //   });
  };
 

  return ModeloZonaCategoria;
};