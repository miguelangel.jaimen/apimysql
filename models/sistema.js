'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sistema = sequelize.define('Sistema', {
    id_sistema: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    nombre_sistema: {type: DataTypes.STRING, unique:'nombre_sistema'},
    tipo_sistema: DataTypes.INTEGER,
    estado: DataTypes.INTEGER,
    eliminado: DataTypes.INTEGER
  },  {tableName:'tbl_sistemas', timestamps: false});
  Sistema.associate = function(models) {
  
  // // VSITAS
  // Sistema.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  // Sistema.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Sistema.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Sistema.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Sistema.hasMany(models.BienUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'bienesUsuario',
  //   });
  };
 

  return Sistema;
};