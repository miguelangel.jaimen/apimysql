'use strict';
module.exports = (sequelize, DataTypes) => {
  const Modelo = sequelize.define('Modelo', {
    id_modelo: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    nombre_modelo: DataTypes.STRING,
    id_proyecto: DataTypes.INTEGER,
  },  {tableName:'tbl_modelos', timestamps: false});
  Modelo.associate = function(models) {
  
  // // VSITAS
  // Modelo.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  Modelo.belongsTo(models.Proyecto, {
    foreignKey: 'id_proyecto',
    as: 'proyecto',
  });

  // // PRIVILEGIOS
    Modelo.hasMany(models.ModeloUbicacion, {
      foreignKey: 'id_modelo',
      as: 'ubicaciones',
    });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Modelo.hasMany(models.ModeloUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Modelo.hasMany(models.ModeloUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'modeloesUsuario',
  //   });
  };
 

  return Modelo;
};