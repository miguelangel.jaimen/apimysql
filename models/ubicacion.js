'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ubicacion = sequelize.define('Ubicacion', {
    codigo_ubicacion: {type: DataTypes.STRING, primaryKey: true},
    nombre_ubicacion: DataTypes.STRING,
  },  {tableName:'tbl_ubicaciones', timestamps: false});
  Ubicacion.associate = function(models) {
  
  // ZONAS
  Ubicacion.hasMany(models.UbicacionZona, {
    foreignKey: 'codigo_ubicacion',
    as: 'zonas',
  });
  
  // // USUARIO
  // Ubicacion.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Ubicacion.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Ubicacion.hasMany(models.UbicacionUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Ubicacion.hasMany(models.UbicacionUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacionesUsuario',
  //   });
  };
 

  return Ubicacion;
};