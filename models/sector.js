'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sector = sequelize.define('Sector', {
    id_sector: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_etapa: DataTypes.STRING,
    nombre_sector: DataTypes.STRING
  },  {tableName:'tbl_sectores', timestamps: false});
  Sector.associate = function(models) {
  
  // // VSITAS
  Sector.hasMany(models.Bien, {
    foreignKey: 'id_sector',
    as: 'bienes',
  });
  
  // // USUARIO
  Sector.belongsTo(models.Etapa, {
    foreignKey: 'id_etapa',
    as: 'etapa',
  });

  // // PRIVILEGIOS
  //   Sector.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Sector.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Sector.hasMany(models.SectorUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'sectoresUsuario',
  //   });
  };
 

  return Sector;
};