'use strict';
module.exports = (sequelize, DataTypes) => {
  const ZonaCategoria = sequelize.define('ZonaCategoria', {
    id_zona_categoria: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    codigo_zona: DataTypes.STRING,
    codigo_categoria: DataTypes.STRING
  },  {tableName:'tbl_zona_categorias', timestamps: false});
  ZonaCategoria.associate = function(models) {
  
  // // ZONA
  ZonaCategoria.belongsTo(models.Zona, {
    foreignKey: 'codigo_zona',
    as: 'zona',
  });
 
  // // UBIACION
  ZonaCategoria.belongsTo(models.Categoria, {
    foreignKey: 'codigo_categoria',
    as: 'categoria',
  });

  // ZonaCategoria.hasMany(models.ZonaCategoria, {
  //   foreignKey: 'id_ubicacion_zona',
  //   as: 'categorias',
  // });
  
  // // // MODELO_UBICACION
  // ZonaCategoria.belongsTo(models.ModeloUbicacion, {
  //   foreignKey: 'id_modelo_ubicacion',
  //   as: 'modelo_ubicacion',
  // });

  // // PRIVILEGIOS
  //   ZonaCategoria.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   ZonaCategoria.hasMany(models.UbicacionZonaUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   ZonaCategoria.hasMany(models.UbicacionZonaUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacion_zonaesUsuario',
  //   });
  };
 

  return ZonaCategoria;
};