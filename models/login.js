const uuid = require('uuid/v4');
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Login = sequelize.define('Login', {
    id_login: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_usuario: DataTypes.STRING,
    correo: DataTypes.STRING,
    clave: DataTypes.STRING,
    fecha_creacion: DataTypes.STRING,
    estado: DataTypes.INTEGER,
    eliminado: DataTypes.INTEGER

  },  {tableName:'tbl_login', timestamps: false});
  Login.associate = function(models) {
  
  // // VSITAS
  // Login.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  // Login.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Login.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Login.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Login.hasMany(models.BienUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'bienesUsuario',
  //   });
  };
 

  return Login;
};