'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bien = sequelize.define('Bien', {
    id_bien: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_sector: DataTypes.STRING,
    numero_bien: DataTypes.INTEGER
  },  {tableName:'tbl_bienes', timestamps: false});
  Bien.associate = function(models) {
  
  // // VSITAS
  Bien.hasMany(models.Extencion, {
    foreignKey: 'id_bien',
    as: 'extenciones',
  });
  
  // // USUARIO
  Bien.belongsTo(models.Sector, {
    foreignKey: 'id_sector',
    as: 'sector',
  });

  // // PRIVILEGIOS
  //   Bien.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Bien.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Bien.hasMany(models.BienUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'bienesUsuario',
  //   });
  };
 

  return Bien;
};