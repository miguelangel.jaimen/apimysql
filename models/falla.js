'use strict';
module.exports = (sequelize, DataTypes) => {
  const Falla = sequelize.define('Falla', {
    codigo_falla: {type: DataTypes.STRING, primaryKey: true},
    nombre_falla: DataTypes.STRING,
  },  {tableName:'tbl_fallas', timestamps: false});
  Falla.associate = function(models) {
  
  // // // // VSITAS
  // Falla.hasMany(models.CategoriaFalla, {
  //   foreignKey: 'id_falla',
  //   as: 'categorias',
  // });
  
  // // USUARIO
  // Falla.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Falla.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Falla.hasMany(models.UbicacionUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Falla.hasMany(models.UbicacionUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'ubicacionesUsuario',
  //   });
  };
 

  return Falla;
};