'use strict';
module.exports = (sequelize, DataTypes) => {
  const Proyecto = sequelize.define('Proyecto', {
    id_proyecto: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    nombre_proyecto: DataTypes.STRING
  },  {tableName:'tbl_proyectos', timestamps: false});
  Proyecto.associate = function(models) {
  
  // // MODELOS
  Proyecto.hasMany(models.Modelo, {
    foreignKey: 'id_proyecto',
    as: 'modelos',
  });
  // // ETAPAS
  Proyecto.hasMany(models.Etapa, {
    foreignKey: 'id_proyecto',
    as: 'etapas',
  });
  
  // // USUARIO
  // Proyecto.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Proyecto.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Proyecto.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Proyecto.hasMany(models.ProyectoUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'proyectoesUsuario',
  //   });
  };
 

  return Proyecto;
};