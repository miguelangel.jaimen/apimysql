
'use strict';

module.exports = (sequelize, DataTypes) => {

  const Usuario = sequelize.define('Usuario', {
    id_usuario: {type: DataTypes.UUID, primaryKey: true, autoIncrement: true},
    nombre_usuario: DataTypes.STRING,
    telefono_usuario: DataTypes.STRING
  },  {tableName:'tbl_usuarios', timestamps: false});
  Usuario.associate = function(models) {
  
  // // VSITAS
  // Usuario.hasMany(models.Visita, {
  //   foreignKey: 'id_usuario_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  // Usuario.belongsTo(models.Usuario, {
  //   foreignKey: 'id_usuario',
  //   as: 'usuario',
  // })

  // // PRIVILEGIOS
  //   Usuario.hasMany(models.Privilegio, {
  //     foreignKey: 'id_usuario_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   Usuario.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_usuario_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   Usuario.hasMany(models.BienUsuario, {
  //     foreignKey: 'id_usuario_usuario',
  //     as: 'bienesUsuario',
  //   });
  };
 

  return Usuario;
};