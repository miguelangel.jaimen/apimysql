'use strict';
module.exports = (sequelize, DataTypes) => {
  const ModeloUbicacion = sequelize.define('ModeloUbicacion', {
    id_modelo_ubicacion: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_modelo: DataTypes.INTEGER,
    codigo_ubicacion: DataTypes.STRING,
    tipo_ubicacion: DataTypes.STRING,
    estado_ubicacion: DataTypes.INTEGER
  },  {tableName:'tbl_modelo_ubicacion', timestamps: false});
  ModeloUbicacion.associate = function(models) {
  
   // // UBICACION
   ModeloUbicacion.belongsTo(models.Ubicacion, {
    foreignKey: 'codigo_ubicacion',
    as: 'ubicacion',
  });
  
  // // MODELO
  ModeloUbicacion.belongsTo(models.Modelo, {
    foreignKey: 'id_modelo',
    as: 'modelo',
  });

  // // UBICACION_ZONAS
    ModeloUbicacion.hasMany(models.ModeloUbicacionZona, {
      foreignKey: 'id_modelo_ubicacion',
      as: 'zonas',
    });
    
  // // PROYECTO_USUARIO_PERFIL
  //   ModeloUbicacion.hasMany(models.ModeloUbicacionUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   ModeloUbicacion.hasMany(models.ModeloUbicacionUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'modelo_ubicacionesUsuario',
  //   });
  };
 

  return ModeloUbicacion;
};