const uuid = require('uuid/v4');
'use strict';

module.exports = (sequelize, DataTypes) => {
  const LoginSistema = sequelize.define('LoginSistema', {
    id_login_sistema: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true},
    id_login: DataTypes.STRING,
    id_sistema: DataTypes.STRING

  },  {tableName:'tbl_login_sistemas', timestamps: false});
  LoginSistema.associate = function(models) {
  
  // // VSITAS
  // LoginSistema.hasMany(models.Visita, {
  //   foreignKey: 'id_login_usuario',
  //   as: 'visitas',
  // });
  
  // // USUARIO
  LoginSistema.belongsTo(models.Login, {
    foreignKey: 'id_login',
    as: 'login',
  })

  // // PRIVILEGIOS
  //   LoginSistema.hasMany(models.Privilegio, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'privilegios',
  //   });
    
  // // PROYECTO_USUARIO_PERFIL
  //   LoginSistema.hasMany(models.ProyectoUsuarioPerfil, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'perfil',
  //   });

  // // BIEN_USUARIO
  //   LoginSistema.hasMany(models.BienUsuario, {
  //     foreignKey: 'id_login_usuario',
  //     as: 'bienesUsuario',
  //   });
  };
 

  return LoginSistema;
};