'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'datview';

exports.validarUsuario = function (req, res, next) {
	if (!req.headers.authorization) {
		return res.status(403).send({ message: 'La peticion no tiene la cabecera esperada' });
	}
	var token = req.headers.authorization.replace(/['"]+/g, '');
	try {
		var payload = jwt.decode(token, secret);
		 console.log('payload', payload);
		// console.log(moment().unix());
		if (payload.exp <= moment().unix()) {
			return res.status(401).send({ message: 'token expirado' });
		}
	} catch (ex) {
		//console.log(ex);
		return res.status(404).send({ message: 'token no valido' });
	}

	req.usuario = payload;

	next();
};
