#API LOGIN CON TOKEN

Este proyecto fue generado para agilizar el proceso de desarrollo de los proyectos se trata de una Api creada a partir de nodeJs, express y sequelize cuenta con autenticacion validada por token.


## Comenzar

1 Clonar el proyecto
```
git clone https://gitlab.com/miguelangel.jaimen/apicontoken.git
```
2 Ingresar a la carpeta creada en la clonación
```
cd basedashboard
```
3 desvincular el proyecto del repositorio base
```
git remote rm origin
```
4 vincular el proyecto al nuevo repositorio
```
git remote add origin url

ejemplo: git remote add origin https://usuario@bitbucket.org/username/repo.git
```
5 Instalar los modulos necesarios mediante npm
```
npm install
```
6 Inicializar el proyecto
```
ng serve
```
