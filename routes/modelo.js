var express = require('express');
var router = express.Router();
const modeloController = require('../controllers').modelo;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', modeloController.test);
router.get('/testToken', md_auth.validarUsuario, modeloController.testToken);
router.get('/listar', modeloController.listar);
router.get('/:id', modeloController.buscar);

// RUTAS POST
router.post('/crear', modeloController.crear);
router.post('/agregarUbicacion', modeloController.agregarUbicacion);
router.post('/agregarZona', modeloController.agregarZona);
router.post('/agregarCategoria', modeloController.agregarCategoria);

// RUTAS PUT
router.put('/:id', modeloController.editar);
router.put('/:id', modeloController.eliminar);

module.exports = router;
