var express = require('express');
var router = express.Router();
const ubicacionController = require('../controllers').ubicacion;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', ubicacionController.test);
router.get('/testToken', md_auth.validarUsuario, ubicacionController.testToken);
router.get('/listar', ubicacionController.listar);
router.get('/:id', ubicacionController.buscar);

// RUTAS POST
router.post('/crear', ubicacionController.crear);

// RUTAS PUT
router.put('/:id', ubicacionController.editar);
router.put('/:id', ubicacionController.eliminar);

module.exports = router;
