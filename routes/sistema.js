var express = require('express');
var router = express.Router();
const sistemaController = require('../controllers').sistema;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', sistemaController.test);
router.get('/testToken', md_auth.validarUsuario, sistemaController.testToken);
router.get('/listar', sistemaController.listar);
router.get('/:id', sistemaController.buscar);

// RUTAS POST
router.post('/crear', sistemaController.crear);

// RUTAS PUT
router.put('/:id', sistemaController.editar);
router.put('/:id', sistemaController.eliminar);

module.exports = router;
