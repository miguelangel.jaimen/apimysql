var express = require('express');
var router = express.Router();
const loginController = require('../controllers').login;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', loginController.test);
router.get('/testToken', md_auth.validarUsuario, loginController.testToken);
router.get('/listar', loginController.listar);
router.get('/:id', loginController.buscar);

// RUTAS POST
router.post('/ingresar', loginController.ingresar);
router.post('/registrar', loginController.registrar);

// RUTAS PUT
router.put('/:id', loginController.editar);
router.put('/:id', loginController.eliminar);

module.exports = router;
