var express = require('express');
var router = express.Router();
const sectorController = require('../controllers').sector;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', sectorController.test);
router.get('/testToken', md_auth.validarUsuario, sectorController.testToken);
router.get('/listar', sectorController.listar);
router.get('/:id', sectorController.buscar);

// RUTAS POST
router.post('/crear', sectorController.crear);

// RUTAS PUT
router.put('/:id', sectorController.editar);
router.put('/:id', sectorController.eliminar);

module.exports = router;
