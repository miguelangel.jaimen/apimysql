var express = require('express');
var router = express.Router();
const zonaController = require('../controllers').zona;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', zonaController.test);
router.get('/testToken', md_auth.validarUsuario, zonaController.testToken);
router.get('/listar', zonaController.listar);
router.get('/:id', zonaController.buscar);

// RUTAS POST
router.post('/crear', zonaController.crear);

// RUTAS PUT
router.put('/:id', zonaController.editar);
router.put('/:id', zonaController.eliminar);

module.exports = router;
