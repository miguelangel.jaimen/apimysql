var express = require('express');
var router = express.Router();
const categoriaController = require('../controllers').categoria;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', categoriaController.test);
router.get('/testToken', md_auth.validarUsuario, categoriaController.testToken);
router.get('/listar', categoriaController.listar);
router.get('/:id', categoriaController.buscar);

// RUTAS POST
router.post('/crear', categoriaController.crear);

// RUTAS PUT
router.put('/:id', categoriaController.editar);
router.put('/:id', categoriaController.eliminar);

module.exports = router;
