var express = require('express');
var router = express.Router();
const proyectoController = require('../controllers').proyecto;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', proyectoController.test);
router.get('/testToken', md_auth.validarUsuario, proyectoController.testToken);
router.get('/listar', proyectoController.listar);
router.get('/:id', proyectoController.buscar);

// RUTAS POST
router.post('/crear', proyectoController.crear);

// RUTAS PUT
router.put('/:id', proyectoController.editar);
router.put('/:id', proyectoController.eliminar);

module.exports = router;
