var express = require('express');
var router = express.Router();
const etapaController = require('../controllers').etapa;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', etapaController.test);
router.get('/testToken', md_auth.validarUsuario, etapaController.testToken);
router.get('/listar', etapaController.listar);
router.get('/:id', etapaController.buscar);

// RUTAS POST
router.post('/crear', etapaController.crear);

// RUTAS PUT
router.put('/:id', etapaController.editar);
router.put('/:id', etapaController.eliminar);

module.exports = router;
