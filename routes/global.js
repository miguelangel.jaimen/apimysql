var express = require('express');
var router = express.Router();
const globalController = require('../controllers').global;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', globalController.test);
router.get('/testToken', md_auth.validarUsuario, globalController.testToken);
router.get('/listarUbicaciones', globalController.listarUbicaciones);
router.get('/listarZonas/:codigo_ubicacion', globalController.listarUbicacionZonas);
router.get('/listarZonas', globalController.listarZonas);
router.get('/listarCategorias/:codigo_zona', globalController.listarZonasCategorias);
router.get('/listarCategorias', globalController.listarCategorias);
router.get('/listarFallas/:codigo_categoria', globalController.listarCategoriasFallas);
router.get('/listarFallas', globalController.listarFallas);
// router.get('/:id', globalController.buscar);

// // RUTAS POST
// router.post('/crear', globalController.crear);

// // RUTAS PUT
// router.put('/:id', globalController.editar);
// router.put('/:id', globalController.eliminar);

module.exports = router;
