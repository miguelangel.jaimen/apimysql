var express = require('express');
var router = express.Router();
const bienController = require('../controllers').bien;
var md_auth = require ('../middelwares/autenticated');
/* GET users listing. */

// RUTAS GET
router.get('/', bienController.test);
router.get('/testToken', md_auth.validarUsuario, bienController.testToken);
router.get('/listar', bienController.listar);
router.get('/:id', bienController.buscar);

// RUTAS POST
router.post('/crear', bienController.crear);

// RUTAS PUT
router.put('/:id', bienController.editar);
router.put('/:id', bienController.eliminar);

module.exports = router;
