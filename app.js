var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


// // Importar archivos rutas
var indexRouter = require('./routes/index');

var loginRouter = require('./routes/login');
var sistemaRouter = require('./routes/sistema');
var proyectoRouter = require('./routes/proyecto');
var etapaRouter = require('./routes/etapa');
var sectorRouter = require('./routes/sector');
var bienRouter = require('./routes/bien');
var modeloRouter = require('./routes/modelo');
var ubicacionRouter = require('./routes/ubicacion');
var zonaRouter = require('./routes/zona');
var categoriaRouter = require('./routes/categoria');
var globalController = require('./routes/global');
var app = express();

// Cabeceras
app.use((req, res, next) =>{
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});





// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Declarar rutas
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/sistema', sistemaRouter);
app.use('/proyecto', proyectoRouter);
app.use('/etapa', etapaRouter);
app.use('/sector', sectorRouter);
app.use('/bien', bienRouter);
app.use('/modelo', modeloRouter);
app.use('/ubicacion', ubicacionRouter);
app.use('/zona', zonaRouter);
app.use('/categoria', categoriaRouter);
app.use('/global', globalController);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

