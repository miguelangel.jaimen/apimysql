'use strict'

const Categoria = require('../models').Categoria;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test categoria');
}

function testToken(req, res) {
    res.status(200).send('test categoria con token');
}

function listar(req, res) {

    return Categoria
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var categoria = new Categoria(req.body);
    categoria.nombre_categoria = categoria.nombre_categoria.toUpperCase(); 
    categoria.estado = 1;
    categoria.eliminado = 0;
    categoria.save()
        .then((data) => res.status(200).send(categoria))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var categoriaId = req.params.id;
    var update = req.body;
    if(update.nombre_categoria){
        update.nombre_categoria = update.nombre_categoria.toUpperCase(); 
    }
    Categoria.update(update, { where: { id_categoria: categoriaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var categoriaId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Categoria.update(update, { where: { id_categoria: categoriaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Categoria.
        findOne({
            where: { id_categoria: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};