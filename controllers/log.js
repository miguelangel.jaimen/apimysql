'use strict'

const Log = require('../models').Log;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test log');
}

function testToken(req, res) {
    res.status(200).send('test log con token');
}

function listar(req, res) {

    return Log
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var log = new Log(req.body);
    log.save()
        .then((data) => res.status(200).send(log))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function nuevoLog(dataLog, tipo){
    console.log(dataLog, tipo);
    console.log('prueba');
    return new Promise((resolve, reject) =>{
    var log = new Log();
    log.contexto = dataLog.contexto;
    log.id_contexto = dataLog.id_contexto;
    log.tipo_log = tipo;
    log.fecha_log = moment().format();
    log.id_usuario = dataLog.id_usuario;
    log.save()
        .then((data) => resolve(data))
        .catch(resolve('error'));
    })
}


function editar(req, res) {
    var logId = req.params.id;
    var update = req.body;
    Log.update(update, { where: { id_log: logId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var logId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Log.update(update, { where: { id_log: logId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Log.
        findOne({
            where: { id_log: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar,
    nuevoLog
};