'use strict'

const Ubicacion = require('../models').Ubicacion;
const UbicacionZona = require('../models').UbicacionZona;
const Zona = require('../models').Zona;
const ZonaCategoria = require('../models').ZonaCategoria;
const Categoria = require('../models').Categoria;
const CategoriaFalla = require('../models').CategoriaFalla;
const Falla = require('../models').Falla;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test bien');
}


function testToken(req, res) {
    res.status(200).send('test bien con token');
}

function listarUbicaciones(req, res) {

    return Ubicacion
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarUbicacionZonas(req, res) {
    var codigo_ubicacion = req.params.codigo_ubicacion;
    return Ubicacion
        .findAll({
            where: { codigo_ubicacion: codigo_ubicacion },
            include:[{
                model: UbicacionZona,
                attributes:['id_ubicacion_zona'],
                as: 'zonas',
                include:[{
                    model: Zona,
                    as: 'zona',
                }]
            }]
        })
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarZonas(req, res) {

    return Zona
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarZonasCategorias(req, res) {
    var codigo_zona = req.params.codigo_zona;
    return Zona
        .findAll({
            where: { codigo_zona: codigo_zona },
            include:[{
                model: ZonaCategoria,
                attributes:['id_zona_categoria'],
                as: 'categorias',
                include:[{
                    model: Categoria,
                    as: 'categoria',
                }]
            }]
        })
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarCategorias(req, res) {

    return Categoria
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarCategoriasFallas(req, res) {
    var codigo_categoria = req.params.codigo_categoria;
    return Categoria
        .findAll({
            where: { codigo_categoria: codigo_categoria },
            include:[{
                model: CategoriaFalla,
                attributes:['id_categoria_falla'],
                as: 'fallas',
                include:[{
                    model: Falla,
                    as: 'falla',
                }]
            }]
        })
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}


function listarFallas(req, res) {

    return Falla
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function listarMatrizTikets(req, res) {

    return Ubicacion
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

// function crear(req, res) {
//     var bien = new Bien(req.body);
//     bien.estado = 1;
//     bien.eliminado = 0;
//     bien.save()
//         .then((data) => res.status(200).send(bien))
//         .catch((error) => { res.status(400).send({ error: error }); });
// }



// function editar(req, res) {
//     var bienId = req.params.id;
//     var update = req.body;
//     Bien.update(update, { where: { id_bien: bienId } })
//         .then((data) => res.status(200).send(update))
//         .catch((error) => { res.status(400).send({ error: error }); });
// }

// function eliminar(req, res) {
//     var bienId = req.params.id;
//     var update = req.body;
//     update.eliminado = 1;
//     Bien.update(update, { where: { id_bien: bienId } })
//         .then((data) => res.status(200).send(update))
//         .catch((error) => { res.status(400).send({ error: error }); });
// }

// function buscar(req, res) {
//     var id = req.params.id;
//     return Bien.
//         findOne({
//             where: { id_bien: id }
//         }
//         )
//         .then((data) => res.status(200).send(data))
//         .catch((error) => { res.status(400).send({ error: error }); });
// }

module.exports = {
    test,
    testToken,
    listarUbicaciones,
    listarUbicacionZonas,
    listarZonas,
    listarZonasCategorias,
    listarCategorias,
    listarCategoriasFallas,
    listarFallas,
};