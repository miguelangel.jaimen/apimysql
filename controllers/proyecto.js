'use strict'

const Proyecto = require('../models').Proyecto;
const LogController = require('./log');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test proyecto');
}

function testToken(req, res) {
    res.status(200).send('test proyecto con token');
}

function listar(req, res) {

    return Proyecto
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var proyecto = new Proyecto(req.body);
    proyecto.nombre_proyecto = proyecto.nombre_proyecto.toUpperCase(); 
    proyecto.estado = 1;
    proyecto.eliminado = 0;
    proyecto.save()
        .then((data) => {
            var dataLog = {};
            dataLog.contexto = 'PROYECTO';
            dataLog.id_contexto = data.id_proyecto;
            LogController.nuevoLog(dataLog, 'CREAR').then(()=>res.status(200).send(data));
        })
        .catch((error) => {
            console.log('bla');
            var dataLog = {};
            dataLog.contexto = 'PROYECTO';
            // dataLog.id_contexto = data.id_proyecto;
            LogController.nuevoLog(dataLog, 'CREAR-ERROR').then(()=>res.status(400).send({ error: error })); 
            });
}



function editar(req, res) {
    var proyectoId = req.params.id;
    var update = req.body;
    if(update.nombre_proyecto){
        update.nombre_proyecto = update.nombre_proyecto.toUpperCase(); 
    }
    Proyecto.update(update, { where: { id_proyecto: proyectoId } })
        .then((data) => {
            var dataLog = {};
            dataLog.contexto = 'PROYECTO';
            dataLog.id_contexto = proyectoId;
            LogController.nuevoLog(dataLog, 'EDITAR').then(()=>res.status(200).send(data));
        })
        .catch((error) => {
            var dataLog = {};
            dataLog.contexto = 'PROYECTO';
            dataLog.id_contexto = proyectoId;
            LogController.nuevoLog(dataLog, 'EDITAR-ERROR').then(()=>res.status(400).send({ error: error })); 
        });
}

function eliminar(req, res) {
    var proyectoId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Proyecto.update(update, { where: { id_proyecto: proyectoId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Proyecto.
        findOne({
            where: { id_proyecto: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};