'use strict'

const Sector = require('../models').Sector;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test sector');
}

function testToken(req, res) {
    res.status(200).send('test sector con token');
}

function listar(req, res) {

    return Sector
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var sector = new Sector(req.body);
    sector.nombre_sector = sector.nombre_sector.toUpperCase(); 
    sector.estado = 1;
    sector.eliminado = 0;
    sector.save()
        .then((data) => res.status(200).send(sector))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var sectorId = req.params.id;
    var update = req.body;
    if(update.nombre_sector){
        update.nombre_sector = update.nombre_sector.toUpperCase(); 
    }
    Sector.update(update, { where: { id_sector: sectorId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var sectorId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Sector.update(update, { where: { id_sector: sectorId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Sector.
        findOne({
            where: { id_sector: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};