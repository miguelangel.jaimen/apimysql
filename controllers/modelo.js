'use strict'

const Modelo = require('../models').Modelo;
const ModeloUbicacion = require('../models').ModeloUbicacion;
const ModeloUbicacionZona = require('../models').ModeloUbicacionZona;
const ModeloZonaCategoria = require('../models').ModeloZonaCategoria;
const Ubicacion = require('../models').Ubicacion;
const Zona = require('../models').Zona;
const Categoria = require('../models').Categoria;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test modelo');
}

function testToken(req, res) {
    res.status(200).send('test modelo con token');
}

function listar(req, res) {

    return Modelo
        .findAll({
            attributes: ['nombre_modelo', 'id_modelo'],
            include: [{
                model: ModeloUbicacion,
                as: 'ubicaciones',
                attributes: ['id_modelo_ubicacion', 'tipo_ubicacion'],
                include: [{
                    model: Ubicacion,
                    as: 'ubicacion'
                  },
                  {
                    model: ModeloUbicacionZona,
                    as: 'zonas',
                    attributes:['id_ubicacion_zona'],
                    include:[{
                        model: Zona,
                        as: 'zona'
                    },{
                        model: ModeloZonaCategoria,
                        as: 'categorias',
                        include:[{
                            model: Categoria,
                            as: 'categoria'
                        }]
                    }]
                  }],
              }]
        })
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var modelo = new Modelo(req.body);
    modelo.nombre_modelo = modelo.nombre_modelo.toUpperCase(); 
    modelo.estado = 1;
    modelo.eliminado = 0;
    modelo.save()
        .then((data) => res.status(200).send(modelo))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var modeloId = req.params.id;
    var update = req.body;
    if(update.nombre_modelo){
        update.nombre_modelo = update.nombre_modelo.toUpperCase(); 
    }
    Modelo.update(update, { where: { id_modelo: modeloId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var modeloId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Modelo.update(update, { where: { id_modelo: modeloId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Modelo.
        findOne({
            where: { id_modelo: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

// wizard
function agregarUbicacion(req, res) {
    var modeloUbicacion = new ModeloUbicacion(req.body); 
    modeloUbicacion.estado_ubicacion = 1;
    modeloUbicacion.eliminado = 0;
    modeloUbicacion.save()
        .then((data) => res.status(200).send(modeloUbicacion))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function agregarZona(req, res) {
    var ModeloUbicacionZona = new ModeloUbicacionZona(req.body); 
    ModeloUbicacionZona.estado_zona = 1;
    ModeloUbicacionZona.eliminado = 0;
    ModeloUbicacionZona.save()
        .then((data) => res.status(200).send(ModeloUbicacionZona))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function agregarCategoria(req, res) {
    var zonaCategoria = new ModeloZonaCategoria(req.body); 
    zonaCategoria.estado_categoria = 1;
    zonaCategoria.eliminado = 0;
    zonaCategoria.save()
        .then((data) => res.status(200).send(zonaCategoria))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar,
    agregarUbicacion,
    agregarZona,
    agregarCategoria
};