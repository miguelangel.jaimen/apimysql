'use strict'

const Sistema = require('../models').Sistema;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var jwt = require('../services/jwt');


function test(req, res) {
    res.status(200).send('test sistema');
}

function testToken(req, res) {
    res.status(200).send('test sistema con token');
}

function listar(req, res) {

    return Sistema
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var sistema = new Sistema(req.body);
    sistema.nombre_sistema = sistema.nombre_sistema.toUpperCase(); 
    sistema.estado = 1;
    sistema.eliminado = 0;
    sistema.save()
        .then((data) => res.status(200).send(sistema))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var sistemaId = req.params.id;
    var update = req.body;
    Sistema.update(update, { where: { id_sistema: sistemaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var sistemaId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Sistema.update(update, { where: { id_sistema: sistemaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Sistema.
        findOne({
            where: { id_sistema: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};