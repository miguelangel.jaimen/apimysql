
const login = require('./login');
const sistema = require('./sistema');
const proyecto = require('./proyecto');
const etapa = require('./etapa');
const sector = require('./sector');
const bien = require('./bien');
const modelo = require('./modelo');
const ubicacion = require('./ubicacion');
const categoria = require('./categoria');
const zona = require('./zona');
const global = require('./global');

module.exports = {
    login,
    sistema,
    proyecto,
    etapa,
    sector,
    bien,
    modelo,
    ubicacion,
    categoria,
    zona,
    global
};