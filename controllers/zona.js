'use strict'

const Zona = require('../models').Zona;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test zona');
}

function testToken(req, res) {
    res.status(200).send('test zona con token');
}

function listar(req, res) {

    return Zona
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var zona = new Zona(req.body);
    zona.nombre_zona = zona.nombre_zona.toUpperCase(); 
    zona.estado = 1;
    zona.eliminado = 0;
    zona.save()
        .then((data) => res.status(200).send(zona))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var zonaId = req.params.id;
    var update = req.body;
    if(update.nombre_zona){
        update.nombre_zona = update.nombre_zona.toUpperCase(); 
    }
    Zona.update(update, { where: { id_zona: zonaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var zonaId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Zona.update(update, { where: { id_zona: zonaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Zona.
        findOne({
            where: { id_zona: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};