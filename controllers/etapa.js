'use strict'

const Etapa = require('../models').Etapa;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test etapa');
}

function testToken(req, res) {
    res.status(200).send('test etapa con token');
}

function listar(req, res) {

    return Etapa
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var etapa = new Etapa(req.body);
    etapa.nombre_etapa = etapa.nombre_etapa.toUpperCase(); 
    etapa.estado = 1;
    etapa.eliminado = 0;
    etapa.save()
        .then((data) => res.status(200).send(etapa))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var etapaId = req.params.id;
    var update = req.body;
    if(update.nombre_etapa){
        update.nombre_etapa = update.nombre_etapa.toUpperCase(); 
    }
    Etapa.update(update, { where: { id_etapa: etapaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var etapaId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Etapa.update(update, { where: { id_etapa: etapaId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Etapa.
        findOne({
            where: { id_etapa: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};