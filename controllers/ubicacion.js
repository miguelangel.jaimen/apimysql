'use strict'

const Ubicacion = require('../models').Ubicacion;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test ubicacion');
}

function testToken(req, res) {
    res.status(200).send('test ubicacion con token');
}

function listar(req, res) {

    return Ubicacion
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var ubicacion = new Ubicacion(req.body);
    ubicacion.nombre_ubicacion = ubicacion.nombre_ubicacion.toUpperCase(); 
    ubicacion.estado = 1;
    ubicacion.eliminado = 0;
    ubicacion.save()
        .then((data) => res.status(200).send(ubicacion))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var ubicacionId = req.params.id;
    var update = req.body;
    if(update.nombre_ubicacion){
        update.nombre_ubicacion = update.nombre_ubicacion.toUpperCase(); 
    }
    Ubicacion.update(update, { where: { id_ubicacion: ubicacionId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var ubicacionId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Ubicacion.update(update, { where: { id_ubicacion: ubicacionId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Ubicacion.
        findOne({
            where: { id_ubicacion: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};