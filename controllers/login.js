'use strict'
const request = require('request');
const Login = require('../models').Login;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var GLOBAL = require('../services/global').GLOBAL;


function test(req, res) {
  res.status(200).send('test login');
}

function testToken(req, res) {
  res.status(200).send('test login con token');
}

function listar(req, res) {

  return Login
    .findAll()
    .then((data) => res.status(200).send({ data }))
    .catch((error) => { res.status(400).send(error); });
}

function ingresar(req, res) {
  request.post(GLOBAL.urlLogin, {form:req.body}, function (error, response, body) {
    if(error) {
      res.status(500).send({mensaje: 'error en la petición'});
    }else {
      if ( body) {
        res.status(200).send(JSON.parse(body)); 
      } else {
        res.status(500).send({mensaje: 'error en la petición'});
      }
    }
    console.log('error:', error); // Print the error if one occurred
    console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    console.log('body:', body); // Print the HTML for the Google homepage.
  });
}



function registrar(req, res) {

  var login = new Login();
  var params = req.body;
  login.id_usuario = 1;
  login.fecha_creacion = moment().format();
  login.estado = 1;
  login.eliminado = 0;
     login.correo = params.correo.toLowerCase();
       login.clave = params.clave;
  if(login.clave) {
    // enciptar clave
    bcrypt.hash(login.clave, null, null, function(err, hash){
      if(err) {
        res.status(500).send({ mensaje: "login no ha sido registrado" });
      } else {
        login.clave = hash;
        console.log(login.dataValues);
        // res.status(200).send({ mensaje: login.dataValues });
        login.save()
        .then((data) => res.status(200).send(login))
        .catch((error) => { res.status(400).send({error:error}); });
      }
    } );
  }
}



function editar(req, res) {
  console.log(req.params);
  console.log(req.body);
  var usuarioId = req.params.id;
  var update = req.body;
  ///
  if(update.clave) {
    // enciptar clave
    bcrypt.hash(update.clave, null, null, function(err, hash){
      if(err) {
        res.status(500).send({ mensaje: "login no ha sido registrado" });
      } else {
        update.clave = hash;
        Login.update(update, {where:{ id_login_usuario : usuarioId}})
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({error:error}); });
      }
    } );
  }
  ///

}

function eliminar(req, res) {
  console.log(req.params);
  console.log(req.body);
  var usuarioId = req.params.id;
  var update = req.body;
  Login.update(update, {where:{ id_login_usuario : usuarioId}})
  .then((data) => res.status(200).send(update))
  .catch((error) => { res.status(400).send({error:error}); });
}

function buscar (req, res){
  var id = req.params.id;
 return Login.
 findOne({
    where: { id_login: id }
  }
 )
 .then((data) => res.status(200).send(data))
 .catch((error) => { res.status(400).send({error:error}); });
}

module.exports = {
  test,
  testToken,
  buscar,
  listar,
  ingresar,
  registrar,
  editar,
  eliminar
};