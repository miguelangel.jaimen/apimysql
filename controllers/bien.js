'use strict'

const Bien = require('../models').Bien;
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

function test(req, res) {
    console.log('pa que veas');
    res.status(200).send('test bien');
}

function testToken(req, res) {
    res.status(200).send('test bien con token');
}

function listar(req, res) {

    return Bien
        .findAll()
        .then((data) => res.status(200).send({ data }))
        .catch((error) => { res.status(400).send(error); });
}

function crear(req, res) {
    var bien = new Bien(req.body);
    bien.estado = 1;
    bien.eliminado = 0;
    bien.save()
        .then((data) => res.status(200).send(bien))
        .catch((error) => { res.status(400).send({ error: error }); });
}



function editar(req, res) {
    var bienId = req.params.id;
    var update = req.body;
    Bien.update(update, { where: { id_bien: bienId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function eliminar(req, res) {
    var bienId = req.params.id;
    var update = req.body;
    update.eliminado = 1;
    Bien.update(update, { where: { id_bien: bienId } })
        .then((data) => res.status(200).send(update))
        .catch((error) => { res.status(400).send({ error: error }); });
}

function buscar(req, res) {
    var id = req.params.id;
    return Bien.
        findOne({
            where: { id_bien: id }
        }
        )
        .then((data) => res.status(200).send(data))
        .catch((error) => { res.status(400).send({ error: error }); });
}

module.exports = {
    test,
    testToken,
    buscar,
    listar,
    crear,
    editar,
    eliminar
};