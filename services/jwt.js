'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'datview';

exports.createToken = function(user) {
    var payload = {
        id: user.id_login,
        correo: user.correo,
        estado: user.estado,
        sistema: user.sistema,
        iat: moment().unix(), //fecha de creación del token en timeStamp
        exp: moment().add(30, 'days').unix() //fecha de expiracion del token
        // exp: moment().add(30, 'days').unix()
    };

    return jwt.encode(payload, secret);
};